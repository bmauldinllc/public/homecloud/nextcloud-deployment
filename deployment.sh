#!/bin/bash

command=$1
export POSTGRES_PASSWORD=$(cat docker-compose/secrets/postgres-password)
export NEXTCLOUD_ADMIN_PASSWORD=$(cat docker-compose/secrets/nextcloud-admin-password)

main()
{
  case $command in

  deploy)
    printf "Deploying...\n"
    cd docker-compose
    docker compose up -d
    cd ..
    ;;

  remove)
    printf "Removing...\n"
    cd docker-compose
    docker compose down
    cd ..
    ;;

  status)
    printf "Status...\n"
    cd docker-compose
    docker compose ls
    cd ..
    ;;

  esac
}

main "$@"
